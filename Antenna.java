
public class Antenna
{
	private int speedLimit;
	private int setPoint;
	private int accelLimit;
	private int decelLimit;
	private int block;
	private double grade;
	private String[] authority;
	private String[] route;
	private String beacon;
	private boolean[] flags;
	
	public Antenna()
	{
		
	}
	
	
	/**
	 * @return the speedLimit
	 */
	public int getSpeedLimit()
	{
		return speedLimit;
	}
	/**
	 * @param speedLimit the speedLimit to set
	 */
	public void setSpeedLimit(int speedLimit)
	{
		this.speedLimit = speedLimit;
	}
	/**
	 * @return the setPoint
	 */
	public int getSetPoint()
	{
		return setPoint;
	}
	/**
	 * @param setPoint the setPoint to set
	 */
	public void setSetPoint(int setPoint)
	{
		this.setPoint = setPoint;
	}
	/**
	 * @return the accelLimit
	 */
	public int getAccelLimit()
	{
		return accelLimit;
	}
	/**
	 * @param accelLimit the accelLimit to set
	 */
	public void setAccelLimit(int accelLimit)
	{
		this.accelLimit = accelLimit;
	}
	/**
	 * @return the decelLimit
	 */
	public int getDecelLimit()
	{
		return decelLimit;
	}
	/**
	 * @param decelLimit the decelLimit to set
	 */
	public void setDecelLimit(int decelLimit)
	{
		this.decelLimit = decelLimit;
	}
	/**
	 * @return the block
	 */
	public int getBlock()
	{
		return block;
	}
	/**
	 * @param block the block to set
	 */
	public void setBlock(int block)
	{
		this.block = block;
	}
	/**
	 * @return the grade
	 */
	public double getGrade()
	{
		return grade;
	}
	/**
	 * @param grade the grade to set
	 */
	public void setGrade(double grade)
	{
		this.grade = grade;
	}
	/**
	 * @return the authority
	 */
	public String[] getAuthority()
	{
		return authority;
	}
	/**
	 * @param authority the authority to set
	 */
	public void setAuthority(String[] authority)
	{
		this.authority = authority;
	}
	/**
	 * @return the route
	 */
	public String[] getRoute()
	{
		return route;
	}
	/**
	 * @param route the route to set
	 */
	public void setRoute(String[] route)
	{
		this.route = route;
	}
	/**
	 * @return the beacon
	 */
	public String getBeacon()
	{
		return beacon;
	}
	/**
	 * @param beacon the beacon to set
	 */
	public void setBeacon(String beacon)
	{
		this.beacon = beacon;
	}
	/**
	 * @return the flags
	 */
	public boolean[] getFlags()
	{
		return flags;
	}
	/**
	 * @param flags the flags to set
	 */
	public void setFlags(boolean[] flags)
	{
		this.flags = flags;
	}
}
