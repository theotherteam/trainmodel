
public class TrainModel
{
	private String TrainID;
	private String line;
	private String Section;
	private String block;
	private int PassNum;
	private double actualVelocity;
	private double power;
	private int brakes;
	private int failure;
	private boolean ebrake;
	private boolean[] flags;
	
	public TrainModel()
	{
		
	}
	
	/**
	 * @return the trainID
	 */
	public String getTrainID()
	{
		return TrainID;
	}
	/**
	 * @param trainID the trainID to set
	 */
	public void setTrainID(String trainID)
	{
		TrainID = trainID;
	}
	/**
	 * @return the line
	 */
	public String getLine()
	{
		return line;
	}
	/**
	 * @param line the line to set
	 */
	public void setLine(String line)
	{
		this.line = line;
	}
	/**
	 * @return the section
	 */
	public String getSection()
	{
		return Section;
	}
	/**
	 * @param section the section to set
	 */
	public void setSection(String section)
	{
		Section = section;
	}
	/**
	 * @return the block
	 */
	public String getBlock()
	{
		return block;
	}
	/**
	 * @param block the block to set
	 */
	public void setBlock(String block)
	{
		this.block = block;
	}
	/**
	 * @return the passNum
	 */
	public int getPassNum()
	{
		return PassNum;
	}
	/**
	 * @param passNum the passNum to set
	 */
	public void setPassNum(int passNum)
	{
		PassNum = passNum;
	}
	/**
	 * @return the actualVelocity
	 */
	public double getActualVelocity()
	{
		return actualVelocity;
	}
	/**
	 * @param actualVelocity the actualVelocity to set
	 */
	public void setActualVelocity(double actualVelocity)
	{
		this.actualVelocity = actualVelocity;
	}
	/**
	 * @return the power
	 */
	public double getPower()
	{
		return power;
	}
	/**
	 * @param power the power to set
	 */
	public void setPower(double power)
	{
		this.power = power;
	}
	/**
	 * @return the brakes
	 */
	public int getBrakes()
	{
		return brakes;
	}
	/**
	 * @param brakes the brakes to set
	 */
	public void setBrakes(int brakes)
	{
		this.brakes = brakes;
	}
	/**
	 * @return the failure
	 */
	public int getFailure()
	{
		return failure;
	}
	/**
	 * @param failure the failure to set
	 */
	public void setFailure(int failure)
	{
		this.failure = failure;
	}
	/**
	 * @return the ebrake
	 */
	public boolean isEbrake()
	{
		return ebrake;
	}
	/**
	 * @param ebrake the ebrake to set
	 */
	public void setEbrake(boolean ebrake)
	{
		this.ebrake = ebrake;
	}
	/**
	 * @return the flags
	 */
	public boolean[] getFlags()
	{
		return flags;
	}
	/**
	 * @param flags the flags to set
	 */
	public void setFlags(boolean[] flags)
	{
		this.flags = flags;
	}
}
